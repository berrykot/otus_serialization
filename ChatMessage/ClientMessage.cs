﻿using System;

namespace ChatMessage
{
	[Serializable]
	public class ClientMessage
	{
		public ClientMessage() { }

		public ClientMessage(string nickName, string userMessage)
		{
			NickName = nickName;
			UserMessage = userMessage;
		}

		public string NickName { get; set; }
		public string UserMessage { get; set; }

		public override string ToString()
		{
			if (string.IsNullOrEmpty(NickName)) return UserMessage;
			else return $"{NickName}: {UserMessage}";
		}
	}
}