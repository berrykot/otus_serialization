﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMessage
{
	[Serializable]
	public class DeliveryMessage
	{
		public DeliveryMessage() { }

		public DeliveryMessage(bool success, string status)
		{
			Success = success;
			Status = status;
		}

		public bool Success { get; set; }
		public string Status { get; set; }
	}
}
