﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatMessage
{
	[Serializable]
	public class ServerMessage
	{
		public ServerMessage() { }

		public ServerMessage(ClientMessage clientMessage)
		{
			ClientMessage = clientMessage;
		}

		public ServerMessage(DeliveryMessage deliveryMessage)
		{
			DeliveryMessage = deliveryMessage;
		}

		public ClientMessage ClientMessage { get; set; }
		public DeliveryMessage DeliveryMessage { get; set; }
	}
}
