﻿using ChatMessage;
using System;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ChatServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        string userName;
        TcpClient client;
        ServerObject server; // объект сервера
        BinaryFormatter formatter;

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
            formatter = new BinaryFormatter();
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                // получаем имя пользователя
                userName = GetInitialMessage().NickName;

                var message = userName + " вошел в чат";
                // посылаем сообщение о входе в чат всем подключенным пользователям
                server.BroadcastMessage(new ClientMessage(null, message), this.Id);
                Console.WriteLine(message);
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        formatter.Serialize(Stream, new ServerMessage(new DeliveryMessage(true, null)));
                        Console.WriteLine(string.Format("{0}: {1}", userName, message));
                        server.BroadcastMessage(new ClientMessage(userName, message), this.Id);
                    }
                    catch (SerializationException ex)
                    {
                        formatter.Serialize(Stream, new ServerMessage(new DeliveryMessage(false, "Ошибка десериализации")));
                    }
                    catch
                    {
                        message = string.Format("{0}: покинул чат", userName);
                        Console.WriteLine(message);
                        server.BroadcastMessage(new ClientMessage(null, message), this.Id);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        private ClientMessage GetInitialMessage()
        {
            return (ClientMessage)formatter.Deserialize(Stream);
        }

        // чтение входящих сообщений
        private string GetMessage()
        {
            StringBuilder builder = new StringBuilder();
            do
            {
                var msg = (ClientMessage)formatter.Deserialize(Stream);
                builder.Append(msg.UserMessage);
            }
            while (Stream.DataAvailable);

            return builder.ToString();
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}