﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace otus_serialization
{
	[Serializable]
	public class Class3 : ISerializable
	{
		//[Никакие аттрибуты не нужны]
		public int MyProperty1 { get; set; }
		public object MyProperty2 { get; set; }

		public Class3() { }
		public Class3(SerializationInfo info, StreamingContext context)
		{
			Console.WriteLine("Derialization by interface ctor");

			MyProperty1 = int.Parse((string)info.GetValue("Prop1", typeof(string)));
			MyProperty2 = new { X = (string)info.GetValue("Prop2.X", typeof(string)) };
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			Console.WriteLine("Serialization by interface method GetObjectData");

			info.AddValue("Prop1", MyProperty1.ToString(), typeof(string));
			info.AddValue("Prop2.X", MyProperty2?.GetType().GetProperty("X")?.GetValue(MyProperty2, null) ?? string.Empty, typeof(string));
		}
		[OnSerializing]//XML серилизатор игнорит все эти аттрибуты
		private void OnSerializing(StreamingContext context)
		{
			Console.WriteLine("OnSerializing");
		}

		[OnSerialized]
		private void OnSerialized(StreamingContext context)
		{
			Console.WriteLine("OnSerialized");
		}

		[OnDeserializing]
		private void OnDeserializing(StreamingContext context)
		{
			Console.WriteLine("OnDeserializing");
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			Console.WriteLine("OnDeserialized");
		}
	}
}
