﻿using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace otus_serialization
{
	//[DataContract] - JSON (Если есть этот аттрибут, то сериализоваться будут только те поляи свойства, которые помечены аттрибутом [DataMember]
	[Serializable] //Используется для bin и json. XML его игнорит
	public class Class1
	{
		[OptionalField]//Только для полей (не для свойств)
		public int MyField1;

		[NonSerialized]//Только для полей (не для свойств) //!!! XML сериализатору плевать на этот аттрибут, он его игнорирует
		[XmlIgnore]
		public object MyField2;

		//[DataMember] - JSON
		public int MyProperty1 { get; set; }
		public string MyProperty2 { get; set; }
		public DateTime MyProperty3 { get; set; }

		[OnSerializing]//XML серилизатор игнорит все эти аттрибуты
		private void OnSerializing(StreamingContext context)
		{
			Console.WriteLine("OnSerializing");
		}

		[OnSerialized]
		private void OnSerialized(StreamingContext context)
		{
			Console.WriteLine("OnSerialized");
		}

		[OnDeserializing]
		private void OnDeserializing(StreamingContext context)
		{
			Console.WriteLine("OnDeserializing");
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			Console.WriteLine("OnDeserialized");
		}
	}
}
