﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace otus_serialization
{
	class Class2SerializationBinder : SerializationBinder
	{
		public override Type BindToType(string assemblyName, string typeName)
		{
			var currentAssFullName = Assembly.GetExecutingAssembly().FullName;

			if (assemblyName == currentAssFullName && typeName == typeof(Class1).FullName)
				return typeof(Class2);

			return null;
		}
	}
}
