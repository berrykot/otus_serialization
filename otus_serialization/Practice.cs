﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;

namespace otus_serialization
{
	class Practice
	{
		BinaryFormatter binaryFormatter;
		XmlSerializer xmlFormatter;
		DataContractJsonSerializer jsonFormatter;
		public Practice()
		{
			binaryFormatter = new BinaryFormatter();
			xmlFormatter = new XmlSerializer(typeof(Class1));
			jsonFormatter = new DataContractJsonSerializer(typeof(Class1));
		}

		public void StartPractice()
		{
			var obj1 = GetClass1Instance();

			Console.WriteLine("Binary standart");
			BinarySerrialize(obj1);
			var obj2 = BinaryDeserrialize();
			var obj2_2 = BinaryDeserrialize_2();//cast to Class2 with binder
			Console.WriteLine();

			Console.WriteLine("Binary by interface ISerializable");
			BinarySerrialize3(new Class3 { MyProperty1 = 5, MyProperty2 = new { X = "113" } });
			var obj3 = BinaryDeserrialize3();
			Console.WriteLine();

			Console.WriteLine("Xml");
			XmlSerrialize(obj1);//Не отработают методы OnSerialized и др
			var obj4 = XmlDeserrialize();
			Console.WriteLine();

			Console.WriteLine("Json");
			JsonSerrialize(obj1);
			var obj5 = JsonDeserrialize();
			Console.WriteLine();
		}

		private void BinarySerrialize(Class1 obj)
		{
			using (var fs = new FileStream("class1.bin", FileMode.OpenOrCreate))
			{
				binaryFormatter.Serialize(fs, obj);
			}
		}

		private Class1 BinaryDeserrialize()
		{
			Class1 obj;
			binaryFormatter.Binder = null;
			using (var fs = new FileStream("class1.bin", FileMode.OpenOrCreate))
			{
				obj = (Class1)binaryFormatter.Deserialize(fs);
			}
			return obj;
		}

		private Class2 BinaryDeserrialize_2()
		{
			Class2 obj = null;
			binaryFormatter.Binder = new Class2SerializationBinder();
			using (var fs = new FileStream("class1.bin", FileMode.OpenOrCreate))
			{
				obj = (Class2)binaryFormatter.Deserialize(fs);
			}
			return obj;
		}

		private void XmlSerrialize(Class1 obj)
		{
			using (var fs = new FileStream("class1.xml", FileMode.OpenOrCreate))
			{
				xmlFormatter.Serialize(fs, obj);
			}
		}

		private Class1 XmlDeserrialize()
		{
			Class1 obj;
			using (var fs = new FileStream("class1.xml", FileMode.OpenOrCreate))
			{
				obj = (Class1)xmlFormatter.Deserialize(fs);
			}
			return obj;
		}

		private void JsonSerrialize(Class1 obj)
		{
			using (var fs = new FileStream("class1.json", FileMode.OpenOrCreate))
			{
				jsonFormatter.WriteObject(fs, obj);
			}
		}

		private Class1 JsonDeserrialize()
		{
			Class1 obj;
			using (var fs = new FileStream("class1.json", FileMode.OpenOrCreate))
			{
				obj = (Class1)jsonFormatter.ReadObject(fs);
			}
			return obj;
		}

		private void BinarySerrialize3(Class3 obj)
		{
			using (var fs = new FileStream("class3.bin", FileMode.OpenOrCreate))
			{
				binaryFormatter.Serialize(fs, obj);
			}
		}

		private Class3 BinaryDeserrialize3()
		{
			Class3 obj;
			binaryFormatter.Binder = null;
			using (var fs = new FileStream("class3.bin", FileMode.OpenOrCreate))
			{
				obj = (Class3)binaryFormatter.Deserialize(fs);
			}
			return obj;
		}

		private Class1 GetClass1Instance()
		{
			return new Class1
			{
				MyField1 = 4,
				MyField2 = new { j = 5 },
				MyProperty1 = 2,
				MyProperty2 = "Text",
				MyProperty3 = DateTime.Today
			};
		}
	}
}
