﻿using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace otus_serialization
{
	[Serializable]
	public class Class2
	{
		public int MyProperty1 { get; set; }
		public string MyProperty2 { get; set; }
		public DateTime MyProperty3 { get; set; }
	}
}
