﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using ChatMessage;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatClient
{
    class Program
    {
        static string userName;
        private const string host = "127.0.0.1";
        private const int port = 8888;
        static TcpClient client;
        static NetworkStream stream;
        static BinaryFormatter formatter;

        static void Main(string[] args)
        {
            client = new TcpClient();
            formatter = new BinaryFormatter();
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                SetUserName();
                SendInitMessage();

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }

        private static void SendInitMessage()
        {
            var initMsg = new ClientMessage(userName, null);
            formatter.Serialize(stream, initMsg);
        }

        private static void SetUserName()
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
        }

        // отправка сообщений
        static void SendMessage()
        {
            while (true)
            {
                var userMsg = Console.ReadLine();
                var msg = new ClientMessage(userName, userMsg);
                formatter.Serialize(stream, msg);
            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    StringBuilder builder = new StringBuilder();
                    do
                    {
                        var msg = (ServerMessage)formatter.Deserialize(stream); 
                        if(msg.ClientMessage != null) builder.Append(msg.ClientMessage.ToString());
                        else if(!msg.DeliveryMessage.Success) builder.Append(msg.DeliveryMessage.Status);
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    if(!string.IsNullOrEmpty(message)) Console.WriteLine(message);//вывод сообщения
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}